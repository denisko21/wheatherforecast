package com.denisloshkarev.weatherforecast.config;

/**
 * Created by Denis Loshkarev.
 */
public class ActivityConfig {
    public static final int REQUEST_CODE_CITY_SELECT = 1;
    public static final int REQUEST_CODE_CITY_ADD = 2;

    public static final String ACTIVITY_TAG_CITY_NAME = "cityName";
    public static final String ACTIVITY_TAG_CITY_ID = "cityId";
    public static final String ACTIVITY_TAG_LATITUDE = "cityLatitude";
    public static final String ACTIVITY_TAG_LONGITUDE = "cityLongitude";

    public static final Double DEFAULT_LATITUDE = 48.4737814;
    public static final Double DEFAULT_LONGITUDE = 24.5852123;
}

