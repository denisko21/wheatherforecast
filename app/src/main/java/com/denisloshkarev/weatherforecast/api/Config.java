package com.denisloshkarev.weatherforecast.api;

/**
 * Created by Denis Loshkarev.
 */
public class Config {
    public static final String APPID = "6549ddea7eea1f5e33c18d552b0c2837";
    public static final String BASE_URL = "http://api.openweathermap.org/data/2.5/";
}
