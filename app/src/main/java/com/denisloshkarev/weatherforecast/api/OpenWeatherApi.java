package com.denisloshkarev.weatherforecast.api;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Denis Loshkarev.
 */
public interface OpenWeatherApi {

    // /weather?lat=35&lon=139
    @GET("weather")
    Call<ForecastToday> getForecastByGPS(@Query("lat") Double lat, @Query("lon") Double lon, @Query("units") String units, @Query("APPID") String appId);

    // /forecast/daily?lat={lat}&lon={lon}&cnt={cnt}
    @GET("forecast/daily")
    Call<Forecast16> getForecast16ByGPS(@Query("lat") Double lat, @Query("lon") Double lon, @Query("units") String units, @Query("cnt") Integer cnt, @Query("APPID") String appId);
}
