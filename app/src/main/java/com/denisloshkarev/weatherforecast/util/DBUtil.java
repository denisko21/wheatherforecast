package com.denisloshkarev.weatherforecast.util;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.denisloshkarev.weatherforecast.model.db.DaoMaster;
import com.denisloshkarev.weatherforecast.model.db.DaoSession;

/**
 * Created by Denis Loshkarev.
 */

public class DBUtil {

    public static DaoSession getSession(Context context){
        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(context, "cities_db", null);
        SQLiteDatabase db = helper.getWritableDatabase();
        DaoMaster daoMaster = new DaoMaster(db);
        return daoMaster.newSession();
    }
}
