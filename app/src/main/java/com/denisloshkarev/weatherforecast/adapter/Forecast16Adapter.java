package com.denisloshkarev.weatherforecast.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.denisloshkarev.weatherforecast.R;
import com.denisloshkarev.weatherforecast.api.ForecastItem;
import com.denisloshkarev.weatherforecast.databinding.Forecast16RowBinding;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Denis Loshkarev.
 */
public class Forecast16Adapter extends RecyclerView.Adapter<Forecast16Adapter.ViewHolder> {

    List<ForecastItem> items;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.forecast_16_row, parent, false);
        return new Forecast16Adapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setItem(items.get(position));
    }

    @Override
    public int getItemCount() {
        return items == null ? 0 : items.size();
    }

    public Forecast16Adapter setItems(List<ForecastItem> items) {
        this.items = items;
        return this;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        Forecast16RowBinding binding;
        Context context;

        public ViewHolder(View itemView) {
            super(itemView);
            context = itemView.getContext();
            binding = DataBindingUtil.bind(itemView);
        }

        public ViewHolder setItem(ForecastItem item) {
            binding.setItem(item);

            ImageView iv = binding.icon;
            String iconName = item.getWeather().getIcon();
            setIcon(iv, iconName);

            return this;
        }

        private void setIcon(ImageView iv, String iconName) {
            DisplayMetrics metrics = context.getResources().getDisplayMetrics();
            int height = 100;
            int size = Math.round(metrics.density * height);
            int img = context.getResources().getIdentifier(iconName, "drawable", context.getPackageName());
            if (img != 0) {
                Picasso.with(context).load(img).resize(size, size).into(iv);
            }
        }
    }
}
