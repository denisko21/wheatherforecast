package com.denisloshkarev.weatherforecast.adapter;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;

import com.denisloshkarev.weatherforecast.R;
import com.denisloshkarev.weatherforecast.databinding.RowCityBinding;
import com.denisloshkarev.weatherforecast.model.db.CityDB;
import com.denisloshkarev.weatherforecast.ui.CitySelectActivity;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by Denis Loshkarev.
 */
public class CityListAdapter extends RecyclerView.Adapter<CityListAdapter.ViewHolder> {

    private List<CityDB> items;
    private Map<Integer, Boolean> checkedItems;
    private CitySelectActivity activity;
    private boolean shouldShowCheckBox;

    public CityListAdapter(CitySelectActivity activity) {
        this.checkedItems = new ConcurrentHashMap<>();
        this.activity = activity;
    }

    public Map<Integer, Boolean> getCheckedItems() {
        return checkedItems;
    }

    public void setShouldShowCheckBox(boolean shouldShowCheckBox) {
        this.shouldShowCheckBox = shouldShowCheckBox;
    }

    public CityListAdapter setItems(List<CityDB> items) {
        this.items = items;
        return this;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(activity)
                .inflate(R.layout.row_city, parent, false);
        return new CityListAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final CityDB city = items.get(position);
        holder.setItem(city);

        final RelativeLayout row = holder.binding.row;
        final CheckBox checkBox = holder.binding.checkBox;

        if (shouldShowCheckBox) {
            checkBox.setVisibility(View.VISIBLE);
        } else {
            checkBox.setVisibility(View.GONE);
            checkBox.setChecked(false);
            checkedItems.remove(position);
        }

        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                int position = holder.getAdapterPosition();
                if (isChecked) {
                    checkedItems.put(position, true);
                } else {
                    checkedItems.remove(position);
                }
                activity.onCheckBoxClick(checkedItems.size());
            }
        });

        row.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                activity.onItemLongClicked();
                checkBox.performClick();
                return true;
            }
        });
        row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.onItemClicked(city);
            }
        });
    }

    @Override
    public int getItemCount() {
        return items == null ? 0 : items.size();
    }

    public interface OnItemClickListener {
        public void onItemClicked(CityDB city);
    }

    public interface OnItemLongClickListener {
        public void onItemLongClicked();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private RowCityBinding binding;

        ViewHolder(View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
        }

        ViewHolder setItem(CityDB item) {
            binding.setCity(item);
            return this;
        }
    }
}
