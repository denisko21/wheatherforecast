package com.denisloshkarev.weatherforecast.ui;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.denisloshkarev.weatherforecast.R;
import com.denisloshkarev.weatherforecast.adapter.CityListAdapter;
import com.denisloshkarev.weatherforecast.config.ActivityConfig;
import com.denisloshkarev.weatherforecast.databinding.ActivityCitySelectBinding;
import com.denisloshkarev.weatherforecast.model.db.CityDB;
import com.denisloshkarev.weatherforecast.model.db.CityDBDao;
import com.denisloshkarev.weatherforecast.util.DBUtil;

import java.util.List;
import java.util.Map;

public class CitySelectActivity extends AppCompatActivity implements CityListAdapter.OnItemLongClickListener, CityListAdapter.OnItemClickListener {

    @SuppressWarnings("FieldCanBeLocal")
    private ActivityCitySelectBinding binding;
    private CityListAdapter mAdapter;

    private ActionMode mActionMode;

    private ActionMode.Callback mActionModeCallback;

    {
        mActionModeCallback = new ActionMode.Callback() {

            // Called when the action mode is created; startActionMode() was called
            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                // Inflate a menu resource providing context menu items
                MenuInflater inflater = mode.getMenuInflater();
                inflater.inflate(R.menu.context_menu_select_city, menu);
                return true;
            }

            // Called each time the action mode is shown. Always called after onCreateActionMode, but
            // may be called multiple times if the mode is invalidated.
            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false; // Return false if nothing is done
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.action_context_delete:
                        deleteItems();
                        mode.finish();
                        return true;
                    default:
                        return false;
                }
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {
                mActionMode = null;
                mAdapter.setShouldShowCheckBox(false);
                mAdapter.notifyDataSetChanged();
            }
        };
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_city_select);
        setTitle(getString(R.string.activity_title_cities));

        mAdapter = new CityListAdapter(this);
        CityDBDao cityDBDao = DBUtil.getSession(this).getCityDBDao();
        List<CityDB> items = cityDBDao.loadAll();
        mAdapter.setItems(items);
        binding.cities.setAdapter(mAdapter);
        binding.notifyChange();
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_CANCELED);
        finish();
    }

    @Override
    public void onItemLongClicked() {
        if (mActionMode == null) {
            mActionMode = this.startSupportActionMode(mActionModeCallback);
            mAdapter.setShouldShowCheckBox(true);
        }
    }

    public void onCheckBoxClick(int selElements) {
        if (mActionMode == null) return;
        if (selElements == 0) {
            mActionMode.setTitle(getString(R.string.activity_title_cities));
        } else mActionMode.setTitle("Selected " + selElements + " elem");
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onItemClicked(CityDB city) {
        if (mActionMode == null) {
            Intent intent = new Intent();
            intent.putExtra(ActivityConfig.ACTIVITY_TAG_CITY_ID, city.getId());
            intent.putExtra(ActivityConfig.ACTIVITY_TAG_CITY_NAME, city.getName());
            intent.putExtra(ActivityConfig.ACTIVITY_TAG_LATITUDE, city.getLatitude());
            intent.putExtra(ActivityConfig.ACTIVITY_TAG_LONGITUDE, city.getLongitude());
            setResult(RESULT_OK, intent);
            finish();
        }
    }

    private void deleteItems() {
        Map<Integer, Boolean> checkedItem = mAdapter.getCheckedItems();
        CityDBDao cityDBDao = DBUtil.getSession(this).getCityDBDao();
        List<CityDB> items = cityDBDao.loadAll();
        for (Integer pos : checkedItem.keySet()) {
            cityDBDao.delete(items.get(pos));
        }
        items = cityDBDao.loadAll();
        mAdapter.setItems(items);
    }
}
