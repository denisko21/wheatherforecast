package com.denisloshkarev.weatherforecast.ui;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.Window;
import android.widget.Toast;

import com.denisloshkarev.weatherforecast.R;
import com.denisloshkarev.weatherforecast.config.ActivityConfig;
import com.denisloshkarev.weatherforecast.model.db.CityDB;
import com.denisloshkarev.weatherforecast.model.db.CityDBDao;
import com.denisloshkarev.weatherforecast.util.DBUtil;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;

public class StartActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private static final String LOG_TAG = "StartActivity";
    private DrawerLayout mDrawerLayout;
    private NavigationView mNavigationView;
    private Double mCityLat;
    private Double mCityLon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
        setContentView(R.layout.activity_start);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mNavigationView = (NavigationView) findViewById(R.id.left_drawer);

        mNavigationView.setNavigationItemSelectedListener(new DrawerItemClickListener());

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.open, R.string.close);
        toggle.setDrawerIndicatorEnabled(true);
        mDrawerLayout.addDrawerListener(toggle);

        SharedPreferences pref = getPreferences(MODE_PRIVATE);
        mCityLat = (double) pref.getFloat(ActivityConfig.ACTIVITY_TAG_LATITUDE, ActivityConfig.DEFAULT_LATITUDE.floatValue());
        mCityLon = (double) pref.getFloat(ActivityConfig.ACTIVITY_TAG_LONGITUDE, ActivityConfig.DEFAULT_LONGITUDE.floatValue());

        if (savedInstanceState == null) {
            selectItem();
        }
    }

    private void selectItem() {
        selectItem(0);
    }
    
    private void selectItem(int itemId) {
        mDrawerLayout.closeDrawer(mNavigationView);

        Fragment fragment = null;
        switch (itemId) {
            case R.id.menu_add_city:
                openAutocompleteActivity();
                break;
            case R.id.menu_select_city:
                Intent intent = new Intent(this, CitySelectActivity.class);
                startActivityForResult(intent, ActivityConfig.REQUEST_CODE_CITY_SELECT);
                break;
            case R.id.menu_loc_this:
                fragment = new MainForecastGPSFragment();
                break;
            default:
                Bundle data = new Bundle();
                if (mCityLon == null || mCityLat == null) {
                    mCityLat = ActivityConfig.DEFAULT_LATITUDE;
                    mCityLon = ActivityConfig.DEFAULT_LONGITUDE;
                }
                data.putDouble(ActivityConfig.ACTIVITY_TAG_LATITUDE, mCityLat);
                data.putDouble(ActivityConfig.ACTIVITY_TAG_LONGITUDE, mCityLon);
                fragment = new MainForecastFragment();
                fragment.setArguments(data);
                break;
        }
        if (fragment != null) {
            android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.replace(R.id.content_frame, fragment);
            transaction.commit();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {

            case ActivityConfig.REQUEST_CODE_CITY_SELECT:
                if (resultCode == RESULT_OK) {
                    mCityLat = data.getDoubleExtra(ActivityConfig.ACTIVITY_TAG_LATITUDE, ActivityConfig.DEFAULT_LATITUDE);
                    mCityLon = data.getDoubleExtra(ActivityConfig.ACTIVITY_TAG_LONGITUDE, ActivityConfig.DEFAULT_LONGITUDE);
                    saveLastCity(mCityLat, mCityLon);
                }
                break;

            case ActivityConfig.REQUEST_CODE_CITY_ADD:
                if (resultCode == RESULT_OK) {
                    Place place = PlaceAutocomplete.getPlace(this, data);
                    mCityLat = place.getLatLng().latitude;
                    mCityLon = place.getLatLng().longitude;
                    saveLastCity(mCityLat, mCityLon);
                    saveCityToDB(place);
                } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                    Status status = PlaceAutocomplete.getStatus(this, data);
                    Log.e(LOG_TAG, "Error: Status = " + status.toString());
                }
                break;
        }
        selectItem();
    }

    private void saveLastCity(Double mCityLat, Double mCityLon) {
        SharedPreferences sharedPreferences = getPreferences(MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putFloat(ActivityConfig.ACTIVITY_TAG_LATITUDE, mCityLat.floatValue());
        editor.putFloat(ActivityConfig.ACTIVITY_TAG_LONGITUDE, mCityLon.floatValue());
        editor.apply();
    }

    private void saveCityToDB(Place place) {
        CityDBDao cityDBDao = DBUtil.getSession(this).getCityDBDao();
        CityDB city = cityDBDao.load(place.getId());
        if (city == null) {
            city = new CityDB();
            city.setId(place.getId());
            city.setName(place.getAddress().toString());
            city.setLatitude(place.getLatLng().latitude);
            city.setLongitude(place.getLatLng().longitude);
            cityDBDao.insert(city);
        } else {
            Toast.makeText(this, "City is already present", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(mNavigationView)) {
            mDrawerLayout.closeDrawer(mNavigationView);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        return false;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        saveLastCity(mCityLat, mCityLon);
        super.onSaveInstanceState(outState);
    }

    private void openAutocompleteActivity() {
        try {
            Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                    .build(this);
            startActivityForResult(intent, ActivityConfig.REQUEST_CODE_CITY_ADD);
        } catch (GooglePlayServicesRepairableException e) {
            GoogleApiAvailability.getInstance()
                    .getErrorDialog(this, e.getConnectionStatusCode(), 0)
                    .show();
        } catch (GooglePlayServicesNotAvailableException e) {
            String message = "Google Play Services is not available: " +
                    GoogleApiAvailability.getInstance().getErrorString(e.errorCode);

            Log.e(LOG_TAG, message);
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        }
    }

    private class DrawerItemClickListener implements NavigationView.OnNavigationItemSelectedListener {
        @Override
        public boolean onNavigationItemSelected(MenuItem item) {
            selectItem(item.getItemId());
            return true;
        }
    }
}
