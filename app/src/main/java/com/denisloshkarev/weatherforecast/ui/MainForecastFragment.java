package com.denisloshkarev.weatherforecast.ui;

import android.app.Activity;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.denisloshkarev.weatherforecast.R;
import com.denisloshkarev.weatherforecast.adapter.Forecast16Adapter;
import com.denisloshkarev.weatherforecast.api.Config;
import com.denisloshkarev.weatherforecast.api.Forecast16;
import com.denisloshkarev.weatherforecast.api.ForecastToday;
import com.denisloshkarev.weatherforecast.api.OpenWeatherApi;
import com.denisloshkarev.weatherforecast.api.Weather;
import com.denisloshkarev.weatherforecast.config.ActivityConfig;
import com.denisloshkarev.weatherforecast.databinding.ForecastMainBinding;
import com.squareup.picasso.Picasso;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainForecastFragment extends Fragment {

    public static final Integer FORECAST_DAYS_COUNT = 16;
    private static final String LOG_TAG = "MainForecastFragment";

    private OpenWeatherApi service;
    private ForecastMainBinding binding;
    private Context context;

    private Callback<ForecastToday> mCallForecastToday;
    private Callback<Forecast16> mCallForecast16;
    private double mCityLon;
    private double mCityLat;

    {
        mCallForecastToday = new Callback<ForecastToday>() {
            @Override
            public void onResponse(Call<ForecastToday> call, Response<ForecastToday> response) {
                ForecastToday forecast = response.body();
                if (forecast != null && forecast.getCod().intValue() == 200) {
                    setForecastTodayImage(forecast);
                    binding.setForecast(forecast);
                } else showAlertDialogService(getString(R.string.error_server_error));
            }

            @Override
            public void onFailure(Call<ForecastToday> call, Throwable t) {
                showAlertDialogService(t.getLocalizedMessage());
            }
        };

        mCallForecast16 = new Callback<Forecast16>() {
            @Override
            public void onResponse(Call<Forecast16> call, Response<Forecast16> response) {
                Forecast16 forecast16 = response.body();
                if (forecast16 != null && forecast16.getCod().intValue() == 200) {
                    Forecast16Adapter adapter = new Forecast16Adapter();
                    adapter.setItems(forecast16.getForecastList());
                    binding.forecast16days.setAdapter(adapter);
                    binding.swiperefresh.setRefreshing(false);
                } else showAlertDialogService(getString(R.string.error_server_error));
            }

            @Override
            public void onFailure(Call<Forecast16> call, Throwable t) {
                showAlertDialogService(t.getLocalizedMessage());
                binding.swiperefresh.setRefreshing(false);
            }
        };
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(inflater, R.layout.forecast_main, container, false);

        context = getActivity();

        binding.swiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                update();
            }
        });

        Bundle data = this.getArguments();
        parseArguments(data);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Config.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        service = retrofit.create(OpenWeatherApi.class);

        update();

        return binding.getRoot();
    }

    private void parseArguments(Bundle data) {
        mCityLon = ActivityConfig.DEFAULT_LONGITUDE;
        mCityLat = ActivityConfig.DEFAULT_LATITUDE;
        if (data != null) {
            if (data.containsKey(ActivityConfig.ACTIVITY_TAG_LONGITUDE)) {
                mCityLon = data.getDouble(ActivityConfig.ACTIVITY_TAG_LONGITUDE);
            }
            if (data.containsKey(ActivityConfig.ACTIVITY_TAG_LATITUDE)) {
                mCityLat = data.getDouble(ActivityConfig.ACTIVITY_TAG_LATITUDE);
            }
        }
    }

    private void update() {
        service.getForecastByGPS(mCityLat, mCityLon, getString(R.string.api_request_units), Config.APPID).enqueue(mCallForecastToday);
        service.getForecast16ByGPS(mCityLat, mCityLon, getString(R.string.api_request_units), FORECAST_DAYS_COUNT, Config.APPID).enqueue(mCallForecast16);
    }

    private void setForecastTodayImage(ForecastToday forecast) {
        Activity activity = getActivity();
        DisplayMetrics metrics = this.getResources().getDisplayMetrics();
        int height = 125;
        int size = Math.round(metrics.density * height);
        ImageView iv = (ImageView) activity.findViewById(R.id.nowIcon);
        Weather weather = forecast.getWeather();
        if (weather != null) {
            String imgName = weather.getIcon();
            int img = getResources().getIdentifier(imgName, "drawable", activity.getPackageName());
            Picasso.with(activity).load(img).resize(size, size).into(iv);
        }
    }

    private void showAlertDialogService(String err) {
        Log.d(LOG_TAG, err);
        new AlertDialog.Builder(context)
                .setTitle("Error")
                .setMessage(err)
                .setPositiveButton("Close", null)
                .show();
    }
}
