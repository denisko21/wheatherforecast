package com.denisloshkarev.weatherforecast.ui;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.denisloshkarev.weatherforecast.R;
import com.denisloshkarev.weatherforecast.adapter.Forecast16Adapter;
import com.denisloshkarev.weatherforecast.api.Config;
import com.denisloshkarev.weatherforecast.api.Forecast16;
import com.denisloshkarev.weatherforecast.api.ForecastToday;
import com.denisloshkarev.weatherforecast.api.OpenWeatherApi;
import com.denisloshkarev.weatherforecast.databinding.ForecastMainBinding;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.squareup.picasso.Picasso;

import java.util.List;

import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.denisloshkarev.weatherforecast.ui.MainForecastFragment.FORECAST_DAYS_COUNT;

public class MainForecastGPSFragment
        extends Fragment
        implements EasyPermissions.PermissionCallbacks,
        LocationListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    private static final String LOG_TAG = "MainForecastGPSFragment";
    private static final float LOCATION_DISTANCE = 100;
    private static final int LOCATION_REQUEST_INTERVAL = 10000;
    private static final int UPDATE_PAUSE_MS = 10000;
    private static final String SAVE_KEY_LOCATION = "location";
    private static final String SAVE_KEY_LAST_UPDATE_TIME = "mLastUpdateTime";
    private static final int REQUEST_CODE_APP_SETTINGS = 400;

    private Callback<ForecastToday> callForecastToday;
    private Callback<Forecast16> callForecast16;

    private OpenWeatherApi service;
    private ForecastMainBinding binding;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private Location mLocation;
    private long mLastUpdateTime;
    private boolean isColdStart;

    {
        isColdStart = true;
        callForecastToday = new Callback<ForecastToday>() {
            @Override
            public void onResponse(Call<ForecastToday> call, Response<ForecastToday> response) {
                ForecastToday forecast = response.body();
                if (forecast != null && forecast.getCod().intValue() == 200) {
                    setForecastTodayImage(forecast);
                    binding.setForecast(forecast);
                } else showAlertDialogService(getString(R.string.error_server_error));
            }

            @Override
            public void onFailure(Call<ForecastToday> call, Throwable t) {
                showAlertDialogService(t.getLocalizedMessage());
            }
        };

        callForecast16 = new Callback<Forecast16>() {
            @Override
            public void onResponse(Call<Forecast16> call, Response<Forecast16> response) {
                Forecast16 forecast16 = response.body();
                if (forecast16 != null && forecast16.getCod().intValue() == 200) {
                    Forecast16Adapter adapter = new Forecast16Adapter();
                    adapter.setItems(forecast16.getForecastList());
                    binding.forecast16days.setAdapter(adapter);
                    binding.swiperefresh.setRefreshing(false);
                } else showAlertDialogService(getString(R.string.error_server_error));
            }

            @Override
            public void onFailure(Call<Forecast16> call, Throwable t) {
                showAlertDialogService(t.getLocalizedMessage());
                binding.swiperefresh.setRefreshing(false);
            }
        };
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(inflater, R.layout.forecast_main, container, false);

        binding.gpsImg.setVisibility(View.VISIBLE);

        binding.swiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                onSwipeToRefresh();
            }
        });

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Config.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        service = retrofit.create(OpenWeatherApi.class);

        updateValuesFromBundle(savedInstanceState);

        return binding.getRoot();
    }

    private void onSwipeToRefresh() {

        long currentTime = System.currentTimeMillis();
        Log.d(LOG_TAG, String.valueOf(mLastUpdateTime));
        Log.d(LOG_TAG, String.valueOf(currentTime - mLastUpdateTime));
        if (currentTime - mLastUpdateTime > UPDATE_PAUSE_MS) {
            mLastUpdateTime = currentTime;
            update();
        } else {
            binding.swiperefresh.setRefreshing(false);
        }
    }

    private void update() {
        if (mLocation != null) {
            Log.d(LOG_TAG, String.valueOf(mLocation));
            Double lat = mLocation.getLatitude();
            Double lon = mLocation.getLongitude();
            service.getForecastByGPS(lat, lon, getString(R.string.api_request_units), Config.APPID).enqueue(this.callForecastToday);
            service.getForecast16ByGPS(lat, lon, getString(R.string.api_request_units), FORECAST_DAYS_COUNT, Config.APPID).enqueue(this.callForecast16);
        }
    }

    private void setForecastTodayImage(ForecastToday forecast) {
        Activity activity = getActivity();
        DisplayMetrics metrics = this.getResources().getDisplayMetrics();
        int height = 125;
        int size = Math.round(metrics.density * height);
        ImageView iv = (ImageView) activity.findViewById(R.id.nowIcon);
        String imgName = forecast.getWeather().getIcon();
        int img = getResources().getIdentifier(imgName, "drawable", activity.getPackageName());
        Picasso.with(activity).load(img).resize(size, size).into(iv);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        try {
            Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

            if (location != null) {
                setLocation(location);
            }
            mLocationRequest = new LocationRequest();
            mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
            mLocationRequest.setInterval(LOCATION_REQUEST_INTERVAL);
            startLocationUpdates();
        } catch (SecurityException se) {
            stopLocationUpdates();
            showToastLocationError();
        }
    }

    private void showToastLocationError() {
        Toast.makeText(getActivity(), getString(R.string.error_toast_text_location_error), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.d(LOG_TAG, String.valueOf(i));
    }

    protected void setLocation(Location location) {
        if (mLocation == null) mLocation = location;
        Log.d(LOG_TAG, "SET_LOCATION: " + location.toString());
        Log.d(LOG_TAG, "DISTANCE: " + mLocation.distanceTo(location));
        if (!isColdStart && mLocation.distanceTo(location) > LOCATION_DISTANCE) {
            mLocation = location;
            update();
        } else if (isColdStart) {
            mLocation = location;
            Log.d(LOG_TAG, "COLD_START: " + location.toString());
            isColdStart = false;
            update();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        setLocation(location);
    }

    @Override
    public void onStart() {
        checkPermissions();
        super.onStart();
    }

    private void checkPermissions() {
        String[] perms = {Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION};
        if (EasyPermissions.hasPermissions(getActivity(), perms)) {
            initGoogleApiClient();
        } else {
            EasyPermissions.requestPermissions(this, "This application needs the ",
                    REQUEST_CODE_APP_SETTINGS, perms);
        }
    }

    public void initGoogleApiClient() {
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .build();
        }
        mGoogleApiClient.connect();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE_APP_SETTINGS) {
            checkPermissions();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        stopLocationUpdates();
    }

    protected void startLocationUpdates() throws SecurityException {
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.requestLocationUpdates(
                    mGoogleApiClient, mLocationRequest, this);
        }
    }

    protected void stopLocationUpdates() {
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(
                    mGoogleApiClient, this);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        startLocationUpdates();
    }

    private void showAlertDialogService(String err) {
        Log.d(LOG_TAG, err);
        new AlertDialog.Builder(getActivity())
                .setTitle("Error")
                .setMessage(err)
                .setPositiveButton("Close", null)
                .show();
    }

    private void showAlertDialogPermission() {
        new AppSettingsDialog.Builder(this, "We really need that permission")
                .setTitle("Permissions")
                .setPositiveButton("OK")
                .setNegativeButton("Cancel", null /* click listener */)
                .setRequestCode(REQUEST_CODE_APP_SETTINGS)
                .build()
                .show();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        stopLocationUpdates();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelable(SAVE_KEY_LOCATION, mLocation);
        outState.putLong(SAVE_KEY_LAST_UPDATE_TIME, mLastUpdateTime);
        super.onSaveInstanceState(outState);
    }

    private void updateValuesFromBundle(Bundle savedInstanceState) {
        if (savedInstanceState != null) {

            if (savedInstanceState.keySet().contains(SAVE_KEY_LOCATION)) {
                mLocation = savedInstanceState.getParcelable(SAVE_KEY_LOCATION);
            }

            if (savedInstanceState.keySet().contains(SAVE_KEY_LAST_UPDATE_TIME)) {
                mLastUpdateTime = savedInstanceState.getLong(
                        SAVE_KEY_LAST_UPDATE_TIME);
            }
            update();
        }
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
        initGoogleApiClient();
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {
        showAlertDialogPermission();
    }
}
