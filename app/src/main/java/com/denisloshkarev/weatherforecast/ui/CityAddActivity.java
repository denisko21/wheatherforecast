package com.denisloshkarev.weatherforecast.ui;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.denisloshkarev.weatherforecast.R;

public class CityAddActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("Add favorite");
        setContentView(R.layout.activity_add_city);
    }
}
