package com.denisloshkarev.dbgenerator;

import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Schema;

public class DataBase {

    public static void main(String[] args) throws Exception {
        Schema schema = new Schema(1, "com.denisloshkarev.weatherforecast.model.db");
        createEntity(schema);
        new DaoGenerator().generateAll(schema, "D:\\java\\android\\projects\\weatherForecast\\app\\src\\main\\java");
    }

    public static void createEntity(Schema schema) {
        Entity city = schema.addEntity("CityDB");
        city.addStringProperty("id").index().primaryKey().unique();
        city.addStringProperty("name").index();
        city.addDoubleProperty("latitude");
        city.addDoubleProperty("longitude");
    }
}
